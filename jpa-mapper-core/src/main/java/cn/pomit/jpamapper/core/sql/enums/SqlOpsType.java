package cn.pomit.jpamapper.core.sql.enums;

public enum SqlOpsType {
    START_WITH("StartWith", " like concat(#{[0]},'%') "),
    CONTAINS("Contains", " like concat(concat('%', #{[0]}),'%') "),
    END_WITH("EndWith", " like concat('%', #{[0]}) "),
    GREATER_THAN("GreaterThan", " > #{[0]} "),
    LESS_THAN("LessThan", " < #{[0]} "),
    GREATER_EQUAL("GreaterEqual", " >= #{[0]} "),
    LESS_EQUAL("LessEqual", " <= #{[0]} "),
    NOT_EQUAL("NotEqual", " <> #{[0]} ");

    private String identify;

    private String ops;

    public String getIdentify() {
        return identify;
    }

    public String getOps() {
        return ops;
    }

    SqlOpsType(String identify, String ops){
        this.identify = identify;
        this.ops = ops;
    }

    public static SqlOpsType getSqlOpsType(String identify){
        for (SqlOpsType item : values()){
            if (item.identify.equals(identify)){
                return item;
            }
        }
        return null;
    }

    public static String getMatchRegs(){
        StringBuilder result = new StringBuilder();
        int count = 0;
        for (SqlOpsType item : values()){
            if(count == 0){
                result.append(item.getIdentify());
            }else{
                result.append("|").append(item.getIdentify());
            }
            count ++;
        }
        return result.toString();
    }
}
